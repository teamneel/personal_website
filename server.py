from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.route('/')
def index(name=None):
    return render_template('index.html', name=name)

@app.route('/newyork')
def newyork(name=None):
    return render_template('map.html', name=name, city='newyork', latitude=40.734449, longitude=-74.005663, zoom=15)

@app.route('/greenville')
def greenville(name=None):
    return render_template('map.html', name=name, city='greenville', latitude=34.8320212, longitude=-82.4431702, zoom=10)

@app.route('/durham')
def durham(name=None):
    return render_template('map.html', name=name, city='durham', latitude=35.9988464, longitude=-78.9468468, zoom=14)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
